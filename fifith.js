function negativeCount(arr) {
  return arr.filter(num => num < 0).length;
}
console.log(negativeCount([4, 3, 2, 9]));
console.log(negativeCount([0, -3, 5, 7]));